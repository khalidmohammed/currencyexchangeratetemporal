package CurrencyExchangeRate;

import io.temporal.client.ActivityCompletionClient;
import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;

public class ExchangeRateWorker
{
    public static void main(String[] args)
    {
        WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
        WorkflowClient client = WorkflowClient.newInstance(service);
        WorkerFactory factory = WorkerFactory.newInstance(client);
        Worker worker = factory.newWorker(Shared.CURRENCY_EXCHANGE_RATE_TASK_QUEUE);
        worker.registerWorkflowImplementationTypes(ExchangeRateWorkflowImpl.class);
        ActivityCompletionClient completionClient = client.newActivityCompletionClient();
        Object[] activities = new Object[]{new ExchangeRateActivityImpl(completionClient), new ExchangeRateInverseActivityImpl()};
        worker.registerActivitiesImplementations(activities);
        factory.start();
    }
}
