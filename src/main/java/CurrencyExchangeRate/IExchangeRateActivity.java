package CurrencyExchangeRate;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface IExchangeRateActivity
{
    @ActivityMethod
    double fetchExchangeRate(String from, String to, double amount);
}
