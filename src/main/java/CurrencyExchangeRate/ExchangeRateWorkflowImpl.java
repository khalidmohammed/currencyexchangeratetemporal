package CurrencyExchangeRate;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;

import java.time.Duration;

public class ExchangeRateWorkflowImpl implements IExchangeRateWorkflow
{
    ActivityOptions activityOptions =
            ActivityOptions.newBuilder().setScheduleToCloseTimeout(Duration.ofSeconds(15)).build();
    private final IExchangeRateActivity exchangeRateActivity = Workflow.newActivityStub(IExchangeRateActivity.class, activityOptions);
    private final IExchangeRateInverseActivity exchangeRateInverseActivity = Workflow.newActivityStub(IExchangeRateInverseActivity.class, activityOptions);

    @Override
    public double getInverseOfExchangeRate()
    {
        String fromCurrency = "QAR";
        String toCurrency = "USD";
        double amount = 2.0;
        double exchangeRate = exchangeRateActivity.fetchExchangeRate(fromCurrency, toCurrency, amount);
        return exchangeRateInverseActivity.getInverse(exchangeRate);
    }
}
