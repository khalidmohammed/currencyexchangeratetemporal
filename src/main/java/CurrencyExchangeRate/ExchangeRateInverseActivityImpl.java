package CurrencyExchangeRate;

public class ExchangeRateInverseActivityImpl implements IExchangeRateInverseActivity
{
    @Override
    public double getInverse(double exchangeRate)
    {
        return 1 / exchangeRate;
    }
    
}
