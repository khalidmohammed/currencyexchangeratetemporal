package CurrencyExchangeRate;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface IExchangeRateInverseActivity
{
    @ActivityMethod
    double getInverse(double exchangeRate);
}
