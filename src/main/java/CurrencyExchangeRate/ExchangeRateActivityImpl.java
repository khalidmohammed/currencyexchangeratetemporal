package CurrencyExchangeRate;

import io.temporal.activity.Activity;
import io.temporal.activity.ActivityExecutionContext;
import io.temporal.client.ActivityCompletionClient;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;

public class ExchangeRateActivityImpl implements IExchangeRateActivity
{
    private final ActivityCompletionClient completionClient;
    private final WebClient webClient = WebClient.create(Vertx.vertx());

    ExchangeRateActivityImpl(ActivityCompletionClient completionClient)
    {
        this.completionClient = completionClient;
    }

    @Override

    public double fetchExchangeRate(String from, String to, double amount)
    {
        ActivityExecutionContext ctx = Activity.getExecutionContext();
        byte[] taskToken = ctx.getTaskToken();
        String api = String.format("/api/v7/convert?q=%s_%s&compact=ultra&apiKey=%s", from, to, Shared.API_KEY);

        webClient.get(80, Shared.HOST_NAME, api)
                .as(BodyCodec.jsonObject())
                .send()
                .onSuccess(response ->
                {
                    try
                    {
                        if (response.statusCode() >= 200 && response.statusCode() < 300)
                        {
                            JsonObject currencyExchangeResponse = response.body();
                            double rate = currencyExchangeResponse.getDouble(from.concat("_").concat(to)) * amount;
                            completionClient.complete(taskToken, rate);
                        }
                    } catch (Exception ex)
                    {
                        System.out.println(ex.getMessage());
                        completionClient.completeExceptionally(taskToken, new Exception(ex.getMessage()));
                    }
                })
                .onFailure(err ->
                {
                    System.out.println(err.getMessage());
                    completionClient.completeExceptionally(taskToken, new Exception(err.getMessage()));
                });
        ctx.doNotCompleteOnReturn();
        return 0.0;
    }

}
