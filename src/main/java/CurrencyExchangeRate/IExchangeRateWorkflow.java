package CurrencyExchangeRate;

import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;

@WorkflowInterface
public interface IExchangeRateWorkflow
{
    @WorkflowMethod
    double getInverseOfExchangeRate();
}
